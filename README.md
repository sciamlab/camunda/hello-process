# HelloProcess Application
Una applicazione estremamente semplice per Camunda 7.8.x e successive

## Documentazione
Nel documento [Hello-Process-Deployment](https://goo.gl/mbR8Qo) è contenuta una descrizione dettagliata della procedura per creare, compilare e installare il processo di esempio HelloProcess e successivamente per effettuare modifiche e reinstallare lo stesso

