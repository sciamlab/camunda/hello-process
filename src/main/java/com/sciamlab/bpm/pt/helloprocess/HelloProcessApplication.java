package com.sciamlab.bpm.pt.helloprocess;
import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("Hello Process App")
public class HelloProcessApplication extends ServletProcessApplication {
  // nessun parametro
}

